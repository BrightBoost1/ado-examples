﻿using System;
using System.Collections.Generic;

namespace ExamplesADO.Models;

public partial class CustomerDemographic
{
    public int Id { get; set; }
    public string CustomerTypeId { get; set; } = null!;

    public string? CustomerDesc { get; set; }

    public virtual ICollection<Customer> Customers { get; } = new List<Customer>();
}
