﻿using ExamplesADO.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamplesADO
{
    public class NorthwindContext : DbContext
    {
        public NorthwindContext() : base(GetOptions())
        {
        }
        public NorthwindContext(DbContextOptions<NorthwindContext> options) : base(GetOptions()) { }

        private static DbContextOptions GetOptions()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            var connectionString = config["ConnectionStrings:Northwind"];
            var options = new DbContextOptionsBuilder();
            options.UseSqlServer(connectionString);
            return options.Options;
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Shipper> Shippers { get; set; }

    }
}
