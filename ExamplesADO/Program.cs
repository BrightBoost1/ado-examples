﻿using ExamplesADO.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System.Data;

namespace ExamplesADO
{
    internal class Program
    {
        static void Main(string[] args)
        {
           NorthwindContext context = new NorthwindContext();
           var allProducts = context.Products.Where(p => p.CategoryId > 0).Select(p => new {
                   p.ProductName,
                   Category = p.Category.CategoryName,
               });
           foreach( var product in allProducts )
            {
                Console.WriteLine(product.ProductName + " " + product.Category);
            }
        }

        public static void AdoConnectionStuff()
        {
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings.json", optional: false,
             reloadOnChange: true);
            IConfiguration config = builder.Build();

            string connStr = config["ConnectionStrings:Northwind"];
            SqlConnection cn = new SqlConnection(connStr);
            cn.Open();
            string sql = "Update Shippers Set CompanyName = 'Bla Fast Pack' Where ShipperID = 1";
            SqlCommand cmd = new SqlCommand(sql, cn);
            int rowsChanged = cmd.ExecuteNonQuery();
            Console.WriteLine($"{rowsChanged} rows changed");
            ParameterizedQuery(cn);
            cn.Close();
        }

        static void ParameterizedQuery(SqlConnection cn)
        {
            int requestedCategory = 1;
            string sql =
             "Select * From Products Where CategoryID = @catID";
            SqlCommand cmd = new SqlCommand(sql, cn);
            SqlParameter param =
             new SqlParameter("@catID", SqlDbType.Int);
            param.Value = requestedCategory;
            cmd.Parameters.Add(param);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Console.WriteLine(dr["ProductName"]);
            }
        }
    }
}