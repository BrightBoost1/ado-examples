﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkMiniProject.Models
{
    internal class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }

        [MaxLength(120)]
        public string Rating { get; set; }
        public Category Category { get; set; }
    }
}
