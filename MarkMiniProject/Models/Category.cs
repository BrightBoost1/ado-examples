﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkMiniProject.Models
{
    internal class Category
    {
        
            public int Id { get; set; }
            public string CategoryName { get; set; }
            public ICollection<Movie> Movies { get; set; }
       
    }
}
